<?php
\Yii::$container->set('app\components\site\interfaces\ISiteModelBuilder', 'app\components\site\modelBuilders\SiteModelBuilder');
\Yii::$container->set('app\components\api\interfaces\IApiRepository', 'app\components\api\repositories\ApiRepository');
\Yii::$container->set('app\components\api\interfaces\IApiDataProvider', 'app\components\api\dataProviders\ApiDataProvider');
\Yii::$container->set('app\components\api\interfaces\IApiModelBuilder', 'app\components\api\modelBuilders\ApiModelBuilder');
\Yii::$container->set('app\components\api\interfaces\IApiHandler', 'app\components\api\handlers\ApiHandler');
