<?php

return [
    'adminEmail' => 'admin@example.com',
    'userApiUrl' => 'test1.job/api',
    'imagePath' => './images/',
    'defaultImage' => 'default.jpg',
    'logPath' => '../runtime/logs/'
];
