<?php
    $this->title = $model->title;
?>
<div class="site-index">
    <?php if (!\Yii::$app->user->isGuest): ?>
        <div class="profile-container" data-action="<?php echo $model->profileLink ?>">
            <form action="<?php echo $model->submitAction ?>" class="profile-form">
                <input type="hidden" name="id" value="<?php echo $model->userId ?>" id="id-input">
                <button type="button" id="delete-picture-button" data-action="<?php echo $model->deletePictureAction ?>">
                    <?php echo $model->text['deletePicture'] ?>
                </button>
                <button>
                    <?php echo $model->text['submit'] ?>
                </button>
                <div class="submit-result"></div>
            </form>
        </div>
    <?php else: ?>
        <div class="caption">
            <?php echo $model->text['caption'] ?>
        </div>
        <a href="<?php echo $model->login ?>">
            <?php echo $model->text['loginLinkTitle'] ?>
        </a>
        <br>
        <a href="<?php echo $model->registration ?>">
            <?php echo $model->text['registrationLinkTitle'] ?>
        </a>
    <?php endif; ?>
</div>
