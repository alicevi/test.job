<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $page \app\components\site\models\LoginPage */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = $page->text['title'];
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?php echo $page->text['caption'] ?></p>

    <?php $form = ActiveForm::begin([
        'action' => [$page->submit],
        'id' => 'login-form',
        'layout' => 'horizontal',
        'options'=>['data-home'=>Yii::$app->homeUrl],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
        ]) ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton($page->text['submit'], ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
            <a href="<?php echo $page->registration ?>">
                <?php echo $page->text['registration'] ?>
            </a>
        </div>

    <?php ActiveForm::end(); ?>
    <div class="submit-login-result"></div>
</div>
