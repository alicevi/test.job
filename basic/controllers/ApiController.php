<?php

namespace app\controllers;

use app\components\api\interfaces\IApiDataProvider;
use app\components\api\interfaces\IApiHandler;
use app\components\api\interfaces\IApiModelBuilder;
use yii\base\ErrorException;
use yii\base\InlineAction;
use yii\db\IntegrityException;
use yii\rest\ActiveController;
use app\models\User;
use app\models\LoginForm;
use app\models\RegisterForm;
use Yii;
use yii\web\UnauthorizedHttpException;

class ApiController extends ActiveController
{
    public $modelClass = 'app\models\User';
    protected $apiDataProvider;
    protected $apiHandler;
    protected $apiModelBuilder;
    protected $request;

    public function __construct(
        $id,
        $module,
        IApiDataProvider $IApiDataProvider,
        IApiHandler $IApiHandler,
        IApiModelBuilder $IApiModelBuilder,
        array $config = []
    ){
        parent::__construct($id, $module, $config);
        $this->apiDataProvider = $IApiDataProvider;
        $this->apiHandler = $IApiHandler;
        $this->apiModelBuilder = $IApiModelBuilder;
        $this->request = Yii::$app->request;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['delete'], $actions['create'], $actions['update'], $actions['view'], $actions['put']);

        return $actions;
    }

    protected function verbs()
    {
        return [
            'view' => ['GET'],
            'update' => ['POST'],
            'delete-picture' => ['POST'],
            'login' => ['POST'],
            'register' => ['POST']
        ];
    }

    public function beforeAction($action)
    {
        $this->checkAccess($action);
        return parent::beforeAction($action);
    }

    /**
     * GET
     * Получает данные для построения профиля пользователя
     * Входной параметр: id (ИД пользователя)
     * Ответ: { username, about, picture (в формате base64) }
     */
    public function actionView()
    {
        $id = $this->request->get('id');

        if (!$id) {
            return false;
        }

        $user = User::findOne($id);
        $imageName = $this->apiDataProvider->getImageById($user->picture_id);
        $profile = $this->apiModelBuilder->getProfileModel($user, $imageName);

        return $profile;
    }

    /**
     * POST
     * Обновляет данные пользователя и отправляет ответ от сервера в формате строки
     * Входные данные: { id, about, picture (в формате base64) }
     * Ответ: { message }
     */

    public function actionUpdate()
    {
        $data = $this->request->post();

        if (empty($data)) {
            return 'Получен пустой запрос';
        }

        return $this->apiHandler->saveProfile($data);
    }

    /**
     * POST
     * Удаляет фотографию пользователя и отправляет ответ от сервера в формате строки
     * Входные данные: { id }
     * Ответ: { message }
     */

    public function actionDeletePicture()
    {
        $data = $this->request->post();

        if (empty($data) || !isset($data['id'])) {
            return 'Получен некорректный запрос';
        }

        $user = User::findOne($data['id']);

        return $this->apiHandler->deleteUserPicture($user);
    }

    /**
     * POST
     * Авторизирует пользователя при помощи формы LoginForm
     */

    public function actionLogin()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return true;
        } else {
            $error = implode(" ",$model->firstErrors);
            throw new UnauthorizedHttpException($error);
        }
    }

    /**
     * POST
     * Регистрирует нового пользователя с помощью формы RegisterForm
     */

    public function actionRegister()
    {
        $model = new RegisterForm();

        if ($model->load(Yii::$app->request->post()) && $model->register()) {
            return true;
        } else {
            throw new IntegrityException('Пользователь с таким именем уже существует.');
        }
    }

    /**
     * @param InlineAction $action
     * @param null $model
     * @param array $params
     * @throws \yii\web\ForbiddenHttpException
     */

    public function checkAccess($action, $model = null, $params = [])
    {
        if ($action->id !== "login" && $action->id !== "register" && Yii::$app->user->isGuest)
        {
            throw new \yii\web\ForbiddenHttpException('Для получения доступа пройдите авторизацию.');
        }
    }
}