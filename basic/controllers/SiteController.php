<?php

namespace app\controllers;

use app\components\site\interfaces\ISiteModelBuilder;
use app\models\LoginForm;
use app\models\RegisterForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

// Контроллер, отвечающий за само приложение

class SiteController extends Controller
{
    protected $siteModelBuilder;

    public function __construct(
        $id,
        $module,
        ISiteModelBuilder $ISiteModelBuilder,
        array $config = []
    ) {
        parent::__construct($id, $module, $config);
        $this->siteModelBuilder = $ISiteModelBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            $model = $this->siteModelBuilder->getUnauthorizedPageModel();
        } else {
            $model = $this->siteModelBuilder->getProfilePageModel(Yii::$app->user->id);
        }

        return $this->render('index', ['model' => $model]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        $page = $this->siteModelBuilder->getLoginPageModel();

        $model->password = '';
        return $this->render('login', [
            'page' => $page,
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new RegisterForm();

        $page = $this->siteModelBuilder->getRegistrationPageModel();

        $model->password = '';
        return $this->render('register', [
            'page' => $page,
            'model' => $model,
        ]);
    }
}
