<?php namespace app\components\api\handlers;

use app\components\api\interfaces\IApiHandler;
use app\components\api\interfaces\IApiRepository;
use app\components\utils\Utils;
use app\models\User;

class ApiHandler implements IApiHandler
{
    protected $apiRepository;
    protected $params;

    public function __construct(IApiRepository $IApiRepository)
    {
        $this->apiRepository = $IApiRepository;
        $this->params = \Yii::$app->params;
    }

    public function saveProfile($data)
    {
        if (isset($data['picture'])) {
            $picture = $data['picture'];
            list($type, $picture) = explode(';', $picture);
            list(, $picture) = explode(',', $picture);
            $picture = base64_decode($picture);

            $path = $this->params['imagePath'];
            $filename = md5(rand()).'.jpg';
            file_put_contents($path.$filename, $picture);
            $data['pictureId'] = $this->apiRepository->saveImage($filename);

            if (!$data['pictureId']) {
                return "Ошибка сохранения изображения";
            }
        }

        if (!$this->apiRepository->saveProfile($data)) {
            return "Ошибка сохранения профиля";
        } else {
            $user = User::findOne($data['id']);
            Utils::log('users', 'Пользователь '.$user->username.' изменил свой профиль.');
            return "Профиль успешно сохранён";
        }
    }

    public function deleteUserPicture($user)
    {
        $fileName = $this->apiRepository->getImageById($user->picture_id);
        $path = $this->params['imagePath'];
        try {
            unlink($path . $fileName);
        } catch (\ErrorException $e) {
            return "Файла для удаления не обнаружено.";
        }


        Utils::log('users', 'Пользователь '.$user->username.' удалил свою фотографию.');
        return $this->apiRepository->deleteUserPicture($user->id, $user->picture_id);
    }
}