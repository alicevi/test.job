<?php namespace app\components\api\models;

class Profile
{
    public $username;
    public $about;
    public $picture;
}