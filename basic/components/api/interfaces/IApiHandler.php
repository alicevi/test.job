<?php namespace app\components\api\interfaces;


interface IApiHandler
{
    public function saveProfile($data);
    public function deleteUserPicture($user);
}