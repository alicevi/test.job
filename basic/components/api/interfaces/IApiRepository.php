<?php namespace app\components\api\interfaces;

interface IApiRepository
{
    public function getImageById($id);
    public function saveImage($image);
    public function deleteUserPicture($id, $pictureId);
    public function saveProfile($data);
}