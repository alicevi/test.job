<?php namespace app\components\api\interfaces;

interface IApiModelBuilder
{
    public function getProfileModel($user, $filePath);
}