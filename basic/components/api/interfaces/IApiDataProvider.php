<?php namespace app\components\api\interfaces;

interface IApiDataProvider
{
    public function getImageById($id);
}