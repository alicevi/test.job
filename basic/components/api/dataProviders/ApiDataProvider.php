<?php namespace app\components\api\dataProviders;

use app\components\api\interfaces\IApiDataProvider;
use app\components\api\interfaces\IApiRepository;

class ApiDataProvider implements IApiDataProvider
{
    protected $apiRepository;

    public function __construct(IApiRepository $IApiRepository)
    {
        $this->apiRepository = $IApiRepository;
    }

    public function getImageById($id)
    {
        return $this->apiRepository->getImageById($id);
    }
}