<?php namespace app\components\api\modelBuilders;

use app\components\api\interfaces\IApiModelBuilder;
use app\components\api\models\Profile;
use yii\base\ErrorException;

class ApiModelBuilder implements IApiModelBuilder
{
    public function getProfileModel($user, $fileName)
    {
        $profile = new Profile();

        $profile->username = $user->username;
        $profile->about = $user->about ?: '';
        $imagePath = \Yii::$app->params['imagePath'];
        try {
            $image = file_get_contents($imagePath.$fileName);
        } catch (ErrorException $e) {
            $defaultImage = \Yii::$app->params['defaultImage'];
            $image = file_get_contents($imagePath.$defaultImage);
        }

        $profile->picture = base64_encode($image);

        return $profile;
    }
}