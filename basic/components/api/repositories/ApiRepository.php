<?php namespace app\components\api\repositories;

use app\components\api\interfaces\IApiRepository;
use yii\db\Query;
use Yii;

class ApiRepository implements IApiRepository
{
    protected $userTable = '{{user}}';
    protected $filesTable = '{{files}}';
    protected $connection;

    public function __construct()
    {
        $this->connection = Yii::$app->db;
    }

    public function getImageById($id)
    {
        if (!$id) {
            return false;
        }

        $query = $this->createBasicQuery($this->filesTable, 'name');

        $fileName = $query
            ->where(['id' => $id])
            ->one();

        if (isset($fileName['name'])) {
            return $fileName['name'];
        } else {
            return false;
        }
    }

    public function saveImage($filename)
    {
        try {
            $this->connection->createCommand()->insert(
                $this->filesTable,
                [
                    'name' => $filename
                ]
            )->execute();
        } catch (\Exception $e) {
            return 'Ошибка сохранения изображения';
        }

        $query = $this->createBasicQuery($this->filesTable, 'id');
        $result = $query
            ->where(['name' => $filename])
            ->one();

        if (empty($result)) {
            return false;
        }

        return $result['id'];
    }

    public function deleteUserPicture($id, $pictureId)
    {
        try {
            $this->connection->createCommand()->update(
                $this->userTable,
                ['picture_id' => null],
                ['id' => $id]
            )->execute();
        } catch (\Exception $e) {
            return "Ошибка удаления фотографии из профиля";
        }

        try {
            $this->connection->createCommand()->delete(
                $this->filesTable,
                ['id' => $pictureId]
            )->execute();
        } catch (\Exception $e) {
            return "Ошибка удаления фотографии";
        }

        return "Фотография успешно удалена";
    }

    public function saveProfile($data)
    {
        $update = [
            'about' => $data['about']
        ];

        if (isset($data['pictureId'])) {
            $update['picture_id'] = $data['pictureId'];
        }

        try {
            $this->connection->createCommand()->update(
                $this->userTable,
                $update,
                ['id' => $data['id']]
            )->execute();
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    private function createBasicQuery($table, $fields = '*')
    {
        $query = new Query();

        return $query
            ->select($fields)
            ->from($table);
    }
}