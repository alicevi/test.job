<?php namespace app\components\site\modelBuilders;

use app\components\site\interfaces\ISiteModelBuilder;
use app\components\site\models\LoginPage;
use app\components\site\models\RegistrationPage;
use app\components\site\models\UnauthorizedPage;
use app\components\site\models\ProfilePage;
use yii\helpers\Url;

class SiteModelBuilder implements ISiteModelBuilder
{
    public function getUnauthorizedPageModel()
    {
        $unauthorizedPage = new UnauthorizedPage();
        $unauthorizedPage->title = 'Пройдите авторизацию';
        $unauthorizedPage->text = [
            'caption' => 'Для изменения профиля, пройдите авторизацию или зарегистрируйтесь:',
            'loginLinkTitle' => 'Авторизация',
            'registrationLinkTitle' => 'Регистрация'
        ];
        $unauthorizedPage->login = Url::to(['site/login']);
        $unauthorizedPage->registration = Url::to(['site/register']);

        return $unauthorizedPage;
    }

    public function getProfilePageModel($id)
    {
        $profilePage = new ProfilePage();
        $profilePage->title = 'Главная страница';
        $profilePage->text = [
            'about' => 'О себе',
            'picture' => 'Ваша фотография',
            'deletePicture' => 'Удалить фотографию',
            'submit' => 'Сохранить'
        ];
        $profilePage->userId = $id;
        $profilePage->profileLink = Url::to(['api/view', 'id' => $id]);
        $profilePage->deletePictureAction = Url::to(['api/delete-picture']);
        $profilePage->submitAction = Url::to(['api/update']);

        return $profilePage;
    }

    public function getLoginPageModel()
    {
        $loginPage = new LoginPage();
        $loginPage->text = [
            'title' => 'Авторизация',
            'caption' => 'Для авторизации заполните следующие поля:',
            'submit' => 'Войти',
            'registration' => 'Регистрация'
        ];
        $loginPage->registration = Url::to(['site/register']);
        $loginPage->submit = Url::to(['api/login']);

        return $loginPage;
    }
    
    public function getRegistrationPageModel()
    {
        $registrationPage = new RegistrationPage();
        $registrationPage->text = [
            'title' => 'Регистрация',
            'caption' => 'Для регистрации заполните следующие поля:',
            'submit' => 'Зарегистрироваться',
            'login' => 'Авторизация'
        ];
        $registrationPage->login = Url::to(['site/login']);
        $registrationPage->submit = Url::to(['api/register']);

        return $registrationPage;
    }
}
