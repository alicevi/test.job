<?php namespace app\components\site\models;

class UnauthorizedPage
{
    public $title;
    public $text;
    public $login;
    public $registration;
}