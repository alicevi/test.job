<?php namespace app\components\site\models;

class ProfilePage
{
    public $title;
    public $text;
    public $userId;
    public $profileLink;
    public $deletePictureAction;
    public $submitAction;
}