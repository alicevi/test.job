<?php namespace app\components\site\interfaces;

interface ISiteModelBuilder
{
    public function getUnauthorizedPageModel();
    public function getProfilePageModel($id);
    public function getLoginPageModel();
    public function getRegistrationPageModel();
}