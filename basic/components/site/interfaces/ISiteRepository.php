<?php namespace app\components\site\interfaces;

interface ISiteRepository
{
    public function getUserDataById($id);
}