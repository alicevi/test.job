<?php namespace app\components\utils;

class Utils
{
    public static function log($file, $message)
    {
        try {
            $logPath = \Yii::$app->params['logPath'];
            $fp = fopen($logPath.$file.'.log', 'a');
            $text = date('Y-m-d h:i:s')." ".$message."\n";
            fwrite($fp, $text);
            fclose($fp);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}