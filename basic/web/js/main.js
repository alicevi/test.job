$(document).ready(function () {
    registerOnLoginSubmit();
    registerOnRegisterSubmit();
    registerOnProfileSubmit();
    registerDeletePictureButtonClick();
});

window.onload = function () {
    loadProfile();
};

function registerOnLoginSubmit() {
    $(document).on('submit', '#login-form', function (e) {
        onLoginSubmit($(this), e);
    });

    function onLoginSubmit(form, e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: form.attr("action"),
            data: form.serialize(),
            success: function (result) {
                window.location.replace(form.attr("data-home"));
            },
            error: function (response) {
                $('.submit-login-result').html(response.responseJSON.message);
            }
        });
    }
}

function registerOnRegisterSubmit() {
    $(document).on('submit', '#register-form', function (e) {
        onRegisterSubmit($(this), e);
    });

    function onRegisterSubmit(form, e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: form.attr("action"),
            data: form.serialize(),
            success: function () {
                window.location.replace(form.attr("data-home"));
            },
            error: function (response) {
                console.log(response);
                $('.submit-register-result').html(response.responseJSON.message);
            }
        });
    }
}

// Страница профиля

function registerOnProfileSubmit() {
    $(document).on('submit', '.profile-form', function (e) {
        onProfileSubmit($(this), e);
    });

    function onProfileSubmit(form, e) {
        e.preventDefault();
        var file = $('#picture-input').prop('files')[0];
        if (file) {
            var fileReader = new FileReader();
            fileReader.readAsDataURL(file);
            fileReader.onloadend = function () {
                var tempImg = new Image();
                tempImg.src = fileReader.result;
                tempImg.onload = function () {

                    var MAX_WIDTH = 200;
                    var MAX_HEIGHT = 200;
                    var tempW = tempImg.width;
                    var tempH = tempImg.height;
                    if (tempW > tempH) {
                        if (tempW > MAX_WIDTH) {
                            tempH *= MAX_WIDTH / tempW;
                            tempW = MAX_WIDTH;
                        }
                    } else {
                        if (tempH > MAX_HEIGHT) {
                            tempW *= MAX_HEIGHT / tempH;
                            tempH = MAX_HEIGHT;
                        }
                    }

                    var canvas = document.createElement('canvas');
                    canvas.width = tempW;
                    canvas.height = tempH;
                    var ctx = canvas.getContext("2d");
                    ctx.drawImage(tempImg, 0, 0, tempW, tempH);

                    var dataURL = canvas.toDataURL("image/jpeg");

                    var fileData = {};
                    fileData.picture = dataURL;
                    $.ajax({
                        type: "POST",
                        url: form.attr("action"),
                        data: form.serialize() + '&' + jQuery.param(fileData),
                        success: function (result) {
                            location.reload();
                        },
                        error: function () {
                            $('.submit-result').html("Ошибка сохранения профиля");
                        }
                    });
                };


            };
        }
    }
}

function registerDeletePictureButtonClick() {
    $(document).on('click', '#delete-picture-button', function () {
        deletePicture($(this));
    });

    function deletePicture(button) {
        var id = $('#id-input');
        var data = {};
        data.id = id.val();

        $.ajax({
            type: "POST",
            url: button.attr("data-action"),
            data: jQuery.param(data),
            success: function (result) {
                location.reload();
            },
            error: function () {
                console.log("Ошибка удаления фотографии");
            }
        });
    }
}

function loadProfile() {
    var container = $('.profile-container');
    if (container.length) {
        $.get(
            container.attr('data-action'), function (result) {
                var image = new Image();
                image.src = 'data:image/png;base64,' + result.picture;
                $('.profile-form').prepend(
                    "<div> Профиль пользователя: " + result.username + "</div>" +
                    "<div> О пользователе:</div>" +
                    "<textarea name='about'>" + result.about + "</textarea>" +
                    "<div> Текущая фотография: </div>" +
                    "<img src='data:image/png;base64," + result.picture + "'>" +
                    "<div>Загрузить новую фотографию:</div>" +
                    "<input type='file' name='picture' id='picture-input' accept='.jpg, .jpeg, .png'>"
                );
            }
        );
    }
}
