<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180329_044622_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->unique(),
            'password' => $this->string(),
            'auth_key' => $this->integer()->unique(),
            'failed_attempts' => $this->integer()->defaultValue(0),
            'locked' => $this->integer()->defaultValue(0)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
