<?php

use yii\db\Migration;

/**
 * Class m180330_095337_update_user_table
 */
class m180330_095337_update_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            '{{user}}',
            'about',
            $this->text()
        );

        $this->addColumn(
            '{{user}}',
            'picture_id',
            $this->integer()
        );

        $this->addForeignKey(
            'fk-avatar-id',
            'user',
            'picture_id',
            'files',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180330_095337_update_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180330_095337_update_user_table cannot be reverted.\n";

        return false;
    }
    */
}
