<?php

namespace app\models;

use app\components\utils\Utils;
use Yii;
use yii\base\Model;

/**
 * RegisterForm is the model behind the register form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RegisterForm extends Model
{
    public $username;
    public $password;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required']
        ];
    }


    /**
     * Register and logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function register()
    {
        $user = new User();
        $user->username = $this->username;
        $user->password = md5($this->password);
        try {
            $user->save();
        } catch (\Exception $e) {
//            return new IntegrityException("Пользователь с таким именем уже существует.");
            return false;
        }
        Utils::log('users', 'Пользователь '.$user->username.' был зарегистрирован.');
        return Yii::$app->user->login($this->getUser());
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
