<?php namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class FilesFixture extends ActiveFixture
{
    public $tableName = '{{%files}}';
}
