<?php

return [
    'user1' => [
        'id' => 1,
        'username' => 'test',
        'password' => 'test',
        'failed_attempts' => 0,
        'locked' => 0,
        'about' => 'Test123',
        'picture_id' => null
    ],
    'user2' => [
        'id' => 2,
        'username' => 'test2',
        'password' => 'test2',
        'failed_attempts' => 4,
        'locked' => 0,
        'about' => 'Test123',
        'picture_id' => 1
    ]
];
