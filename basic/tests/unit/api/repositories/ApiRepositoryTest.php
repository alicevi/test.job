<?php namespace app\tests\unit\api\repository;

use app\tests\fixtures\FilesFixture;
use Codeception\Test\Unit;
use app\components\api\repositories\ApiRepository;
use app\tests\fixtures\UserFixture;
use Yii;

class ApiRepositoryTest extends Unit
{
    use \Codeception\Specify;

    /**
     * @var ConfigRepository
     */
    protected $repository;

    public function _fixtures()
    {
        return [
            'user' => UserFixture::class,
            'files' => FilesFixture::class
        ];
    }

    protected $testApi = [
        'regularFile' => [
            'id' => '1',
            'name' => 'test.jpg',
        ],
        'notFound' => [
            'id' => '_notFound_',
            'code' => '_notFound_',
        ],
        'null' => [
            'id' => null,
            'code' => null,
        ],
    ];

    protected function setUp()
    {
        $this->repository = $this->getRepository();
    }

    public function testGetImageById()
    {
        $this->specify('should find image name', function ($key) {
            $example = $this->testApi[$key];
            $name = $this->repository->getImageById($example['id']);
            $this->assertNotFalse($name);
            $this->assertEquals($this, $example['name']);

        }, [
            'examples' => [
                ['regular'],
            ],
        ]);

        $this->specify('should not find image name', function ($key) {
            $example = $this->testApi[$key];
            $name = $this->repository->getImageById($example['id']);
            $this->assertFalse($name);
        }, [
            'examples' => [
                ['notFound'],
                ['null'],
            ],
        ]);
    }

    /* HELPERS */

    private function getRepository()
    {
        return new ApiRepository();
    }

}
